# 点可云进销存V5

#### 介绍
点可云进销存系统，基于thinkphp+layui开发。功能包含：采购、销售、零售、多仓库管理、财务管理等功能 和超详细的报表功能（采购报表、销售报表、零售报表、仓库报表、资金报表等） 欢迎添加官网qq群：280470323 客服qq：29139260 客服微信：diycloud 商业版官网：www.nodcloud.com

![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/nod1.png)

## 点可云ERP-开源进销存系统
:exclamation: :exclamation: :exclamation: 请注意：这是商业版本更新替换下来的。 纯上一个商业版本。

##  系统演示
http://v5.yimiaonet.com/ 帐号：admin 密码：admin888

##  V6开源版地址
[https://gitee.com/yimiaoOpen/nodcloud](https://gitee.com/yimiaoOpen/nodcloud)

##  另有V7全新商业版
https://erp.nodcloud.com/ V7全新商业版（V7版是付费的 售价288起） 采用tp+vue开发，速度更流畅，功能更齐全。

##  联系我们
官网qq群：
[点可云软件中心](https://jq.qq.com/?_wv=1027&k=tho1GSKV)

官网客服qq：29139260 ， 客服微信：diycloud


##  介绍
点可云进销存系统，基于thinkphp+layui开发。 功能包含：采购、销售、零售、多仓库管理、财务管理等功能 和超详细的报表功能（采购报表、销售报表、零售报表、仓库报表、资金报表等）

##  软件架构
thinkphp+layui
## 截图演示
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220730121622.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220730121622.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220730121715.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E6%8A%A5%E8%A1%A8.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E6%9C%8D%E5%8A%A1%E8%AE%A2%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E7%A7%AF%E5%88%86%E5%85%91%E6%8D%A2%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E8%AE%BE%E7%BD%AE.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E8%B5%84%E9%87%91.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%87%87%E8%B4%AD%E9%80%80%E8%B4%A7%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%94%80%E8%B4%A7%E5%8D%95.png)
![输入图片说明](%E9%A2%84%E8%A7%88%E6%88%AA%E5%9B%BE/%E9%94%80%E8%B4%A7%E9%80%80%E8%B4%A7%E5%8D%95.png)